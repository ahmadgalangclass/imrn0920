import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import skillData from './skillData.json';
import Skill from './components/Skill';

export default class SkillScreen extends Component {
    render(){
        return(

            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('./images/logo.png')} style={{width: 188, height: 50, left: 178}}/>
                </View>

                <View style={styles.user}>
                    <Icon style={styles.userIcon} name='user-circle' size={26} />
                    <View style={styles.userName}>
                        <Text style={{fontSize: 12, lineHeight: 14}}>Hai,</Text>
                        <Text style={{fontSize: 16, lineHeight: 19, color: '#003366'}}>Ahmad Galang Satria</Text>
                    </View>
                </View>

                <Text style={{fontSize: 36, lineHeight: 42, color: '#003366', marginLeft: 16}}>SKILL</Text>
                <View style={{borderBottomWidth: 4, marginLeft: 16, marginRight: 16, borderBottomColor: '#3EC6FF'}}></View>

                <View style={styles.category}>
                    <View style={styles.subCategory}>
                        <Text style={{fontSize: 11.5, lineHeight: 14, color: '#003366', fontWeight: 'bold', marginTop: 9, marginBottom: 9, marginLeft: 8, marginRight: 6}}>Library / Framework </Text>
                    </View>
                    <View style={styles.subCategory}>
                        <Text style={{fontSize: 11.5, lineHeight: 14, color: '#003366', fontWeight: 'bold', marginTop: 9, marginBottom: 9, marginLeft: 8, marginRight: 8}}>Bahasa Pemrograman</Text>
                    </View>
                    <View style={styles.subCategory}>
                        <Text style={{fontSize: 11.5, lineHeight: 14, color: '#003366', fontWeight: 'bold', marginTop: 9, marginBottom: 9, marginLeft: 8, marginRight: 8}}>Teknologi</Text>
                    </View>
                </View>

                <FlatList
                data={skillData.items}
                renderItem={(skill) => <Skill skill={skill} />}
                keyExtractor={(item) => item.id}
                />

            </View>
        )
    }
}

const styles=StyleSheet.create({
    container: {
        marginTop : 10,
        flex: 1
    },
    user: {
        marginTop: 3,
        marginLeft: 16,
        marginRight: 180,
        marginBottom: 16,
        flexDirection: 'row'
    },
    userIcon: {
        marginTop: 4,
        marginLeft: 3,
        marginRight: 11,
        color: '#3EC6FF'
    },
    category: {
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 3
    },  
    subCategory: {
        backgroundColor: '#B4E9FF',
        marginHorizontal: 3,
        borderRadius: 10
    }
})
