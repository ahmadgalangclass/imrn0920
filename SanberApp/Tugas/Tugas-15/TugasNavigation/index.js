import React, { Component } from 'react'
import { Text, View } from 'react-native'
import LoginScreen from './LoginScreen'
import AboutScreen from './AboutScreen'
import SkillScreen from './SkillScreen'
import ProjectScreen from './ProjectScreen'
import AddScreen from './AddScreen'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'

const Root = createStackNavigator()
const RootScreen = () => (
    <Root.Navigator>
        <Root.Screen name="LoginScreen" component={LoginScreen} options={{title: "Login"}} />
        <Root.Screen name="DrawerScreen" component={DrawerScreen} options={{title: "App"}} />
    </Root.Navigator>
)

const Drawer = createDrawerNavigator()
const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="TabsScreen" component={TabsScreen} options={{title: "Tabs"}} />
        <Drawer.Screen name="AboutScreen" component={AboutScreen} options={{title: "About"}} />
    </Drawer.Navigator>
)

const Tabs = createBottomTabNavigator()
const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="SkillScreen" component={SkillScreen} options={{title: "Skill"}} />
        <Tabs.Screen name="ProjectScreen" component={ProjectScreen} options={{title: "Project"}} />
        <Tabs.Screen name="AddScreen" component={AddScreen} options={{title: "Add"}} />
    </Tabs.Navigator>
)

export default class Index extends Component {
    render() {
        return (
            <NavigationContainer>
                <RootScreen />
            </NavigationContainer>
        )
    }
}
