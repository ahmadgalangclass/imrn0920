import React, { Component } from 'react';
import { StackNavigator, withNavigation } from 'react-navigation';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  Button,
  Image,
  TouchableOpacity,
  
} from 'react-native';
import Constants from 'expo-constants';
import { Card } from 'react-native-paper';
import home from './HomeScreen';

// const routes = {
//  home: { screen: home },
// };

export default class HomeScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{flexDirection: 'row', alignSelf:'left'}}>
          <UselessTextInputMultiline />
          <Image
                  style={styles.icon}
                  source={require('./images/icon_notif.png')}
                  resizeMode="contain"
          />   
        </View>
        <Image
            style={{height: 160, width: 260, alignSelf: 'center'}}
            source={require('./images/advertisment_01.png')}
            resizeMode="contain"
        />
        <Image
            style={{height: 50, width: 240, alignSelf: 'center'}}
            source={require('./images/sub_menu_catagory.png')}
            resizeMode="contain"
        />
        
        <Image
            style={{height: 160, width: 260, alignSelf: 'center'}}
            source={require('./images/adv_flash_sell.png')}
            resizeMode="contain"
        />
        <Image
            style={{height: 160, width: 260, alignSelf: 'center'}}
            source={require('./images/adv_new_product.png')}
            resizeMode="contain"
        />

        
        
      </View>
    );
  }
}

/*------------------------------*/
const UselessTextInput = (props) => {
  return (
    <TextInput
      {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
      editable
      maxLength={40}
      style={styles.textInput}
      onChangeText={(text) => this.setState({ text })}
    />
  );
}

const UselessTextInputMultiline = () => {
  const [value, onChangeText] = React.useState('');

  return (
    <View
      style={{
        backgroundColor: 'white',
        borderBottomColor: '#d500f9',
        borderColor: '#d500f9',
        marginTop: 10,
        marginBottom: 15,
      }}>
      <UselessTextInput
        multiline
        numberOfLines={2}
        onChangeText={(text) => onChangeText(text)}
        value={value}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: 'white',
    padding: 10,
  },

  paragraphMedium: {
    margin: 0,
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'left',
    borderColor: '#d500f9',
    padding: 5,
  },
  paragraphSmall: {
    margin: 0,
    fontSize: 10,
    textAlign: 'left',
    borderColor: '#d500f9',
    padding: 5,
  },

  card: {
    borderRadius: 15,
    padding: 10,
    margin: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },

  columnView_1: {
    marginTop: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 15,
  },
  
  image: {
    width: 10,
    height: 10,
    alignSelf: 'center',
  },

  icon: {
    width: 24,
    height: 24,
    padding: 5,
    alignSelf: 'center',
  },

  textInput: {
    paddingRight: 10,
    paddingLeft: 10,
    height: 25,
    width:240,
    borderBottomColor: 'gray',
    borderRadius: 15,
    borderWidth: 1,
  },
  color_1:{
    backgroundColor:'#F77866',
  },

  color_2:{
    backgroundColor:'white',
  },

  buttonShape_1: {
    marginTop: 5,
    marginBottom: 5,
    marginLeft:10,
    marginRight:10,

    paddingTop:10,
    paddingBottom:10,
    
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#fff',

    width: 240,
    height: 40
  },
  buttonShape_2: {
    marginTop: 5,
    marginBottom: 5,
    marginLeft:10,
    marginRight:10,

    paddingTop:5,
    paddingBottom:5,
    
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#fff',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    width: 110,
    height: 30
  },
  
});
