import React, { Component } from 'react';
import { StackNavigator, withNavigation } from 'react-navigation';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  Button,
  Image,
  TouchableOpacity,
  
} from 'react-native';
import Constants from 'expo-constants';
import { Card } from 'react-native-paper';
import home from './HomeScreen';

const routes = {
 home: { screen: home },
};

export default class Register extends React.Component {
  render() {
    return (
      <View style={styles.container}>

        <Text style={styles.paragraphMedium}>Welcome</Text>
        <Text style={styles.paragraphSmall}>Sign Up to Continue</Text>

        <Card style={styles.card}>
          <Text style={{fontSize: 12}}>Name</Text>
          <UselessTextInputMultiline />
          <Text style={{fontSize: 12}}>Email</Text>
          <UselessTextInputMultiline />
          <Text style={{fontSize: 12}}>Phone Number</Text>
          <UselessTextInputMultiline />
          <Text style={{fontSize: 12}}>Password</Text>
          <UselessTextInputMultiline />

        </Card>

        <View style={styles.columnView_1}>

          <TouchableOpacity
            style={[styles.buttonShape_1, styles.color_1,]}
            activeOpacity = { .5 }
            onPress={() => this.props.navigation.navigate('home') }>
              <Text style={{
                textAlign: "center",
                color: 'white'
              }}> Sign In </Text>
          </TouchableOpacity>

          
        </View>
        <View style={{flexDirection: 'row', alignSelf:'center'}}>
            <Text style={{textAlign: "center", fontSize: 8}}> Already Have Account? </Text>
            <Text style={{textAlign: "center", fontSize: 8, color: 'red'}}> Sign In </Text>
        </View>
        
      </View>
    );
  }
}

/*------------------------------*/
const UselessTextInput = (props) => {
  return (
    <TextInput
      {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
      editable
      maxLength={40}
      style={styles.textInput}
      onChangeText={(text) => this.setState({ text })}
    />
  );
}

const UselessTextInputMultiline = () => {
  const [value, onChangeText] = React.useState('');

  return (
    <View
      style={{
        backgroundColor: 'white',
        borderBottomColor: '#d500f9',
        borderColor: '#d500f9',
        marginTop: 10,
        marginBottom: 15,
      }}>
      <UselessTextInput
        multiline
        numberOfLines={2}
        onChangeText={(text) => onChangeText(text)}
        value={value}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: 'white',
    padding: 10,
  },

  paragraphMedium: {
    margin: 0,
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'left',
    borderColor: '#d500f9',
    padding: 5,
  },
  paragraphSmall: {
    margin: 0,
    fontSize: 10,
    textAlign: 'left',
    borderColor: '#d500f9',
    padding: 5,
  },

  card: {
    borderRadius: 15,
    padding: 10,
    margin: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },

  columnView_1: {
    marginTop: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 15,
  },

  textInput: {
    paddingRight: 10,
    paddingLeft: 10,
    height: 25,
    borderBottomColor: 'gray',
    borderRadius: 0,
    borderBottomWidth: 1,
  },
  color_1:{
    backgroundColor:'#F77866',
  },

  buttonShape_1: {
    marginTop: 5,
    marginBottom: 5,
    marginLeft:10,
    marginRight:10,

    paddingTop:10,
    paddingBottom:10,
    
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#fff',

    width: 240,
    height: 40
  },
  
});
