import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default function Skill(props) {

    return (
        < View style={styles.skillContainer}>
            <Icon name={props.skill.item.iconName} size={80} color="#003366"></Icon>
            <View style={styles.skillDetail}>
                <Text style={{ color: "#003366", fontWeight: 'bold', fontSize: 24}}>
                    {props.skill.item.skillName}
                    </Text>
                <Text style={{ color: "#3EC6FF", fontSize: 16, alignContent:'center', textAlign:'left'}}>
                    {props.skill.item.categoryName}
                    </Text>
                <Text style={{ fontSize: 35, color: 'white', alignSelf: 'flex-end', fontWeight: 'bold', marginTop: -5}}>
                    {props.skill.item.percentageProgress}
                    </Text>
            </View>
            <Icon name="chevron-right" size={65} color="#003366"/>
        </View>
    )
}

const styles = StyleSheet.create({
    skillContainer: {
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: "#B4E9FF",
        elevation: 5,
        borderRadius: 10,
        // justifyContent: "space-between",
        alignItems: 'center',
        marginBottom: 8,
        marginLeft: 16,
        marginRight: 16
    },
    skillDetail: {
        display: 'flex',
        alignItems: 'flex-start',
        flex:1
    }
})
