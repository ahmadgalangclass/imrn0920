// import { StatusBar } from 'expo-status-bar';
import * as React from 'react';
import Constant from 'expo-constants'
import { StyleSheet, Text, View } from 'react-native';
// import YoutubeUI from './Tugas/Tugas-12/App';
// import SanberLog from './Tugas/Tugas-13/LoginScreen';
// import SanberAbout from './Tugas/Tugas-13/AboutScreen';
// import Tugas14 from './Tugas/Tugas-14/SkillScreen';
// import AppIndex from "./Tugas/Tugas15/index";
// import Index from './Tugas/Tugas15/TugasNavigation/index'
import Login from './Tugas/Quiz-3/Login';
import Register from './Tugas/Quiz-3/Register';
import Home from './Tugas/Quiz-3/HomeScreen';
import Quiz3 from './Tugas/Quiz-3/Splash'

// const statusBarHeight = StatusBar.currentHeight;

import { Card } from 'react-native-paper';
export default function App() {
  return (
    <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" >
          <Stack.Screen name='Login' component={Login} options={{ headerShown: false }} />
          <Stack.Screen name='Home' component={Home} options={{ headerTitle: 'Home' }} />
        </Stack.Navigator>
      </NavigationContainer>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

