
//==if-else==
var nama = "Jane"
var peran = "Werewolf"
if (nama == '' && peran == '') {
    console.log("Nama harus diisi!");
} else if (nama != '' && peran == '') {
    console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
} else if(nama != '' && peran == 'Penyihir'){
    console.log("Selamat datang di Dunia Werewolf, "+nama);
    console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
}else if (nama != '' && peran == 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, "+nama);
    console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
}else if(nama != '' && peran == 'Werewolf'){
    console.log("Selamat datang di Dunia Werewolf, "+nama);
    console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!" );
}else{
    console.log("Peran tidak dikenal");
}
console.log();

//==Switch case==
var tanggal = 21;
var bulan = 9;
var tahun = 1994;

switch(bulan){
 case 1: {bulan = 'Januari';break; }
 case 2: {bulan = 'February';break; }
 case 3: {bulan = 'Maret';break; }
 case 4: {bulan = 'April';break; }
 case 5: {bulan = 'Mei';break; }
 case 6: {bulan = 'Juni';break; }
 case 7: {bulan = 'Juli';break; }   
 case 8: {bulan = 'Agustus';break; }
 case 9: {bulan = 'September';break; }
 case 10: {bulan = 'Oktober';break; }
 case 11: {bulan = 'November';break; }
 case 12: {bulan = 'Desember';break; }
 default: {console.log('Tanggal Tidak Ditemukan')}
}

console.log(tanggal +" "+ bulan + " " +tahun);

