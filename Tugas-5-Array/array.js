//==SOAL No.1==
function range(startNum, finishNum) {
    var arr = [];
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            arr.push(i);
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i--) {
            arr.push(i);
        }
    } else if (startNum == null || finishNum == null) {
        return -1
    }
    return arr;
}

console.log("==Soal No. 1==")
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())
console.log();


//==SOAL No.2==
function rangeWithStep(startNum, finishNum, step) {
    var arr = [];
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i += step) {
            arr.push(i);
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i -= step) {
            arr.push(i);
        }
    }
    return arr;
}

console.log("==Soal No. 2==")
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))
console.log();


//==SOAL No.3==
function sum(startNum, finishNum, step = 1) {
    var sum = 0;
    if (startNum < finishNum) {
      for (var i = startNum; i <= finishNum; i += step) {
        sum = sum + i;
      }
    } else if (startNum > finishNum) {
      for (var i = startNum; i >= finishNum; i -= step) {
        sum = sum + i;
      }
    } else if (startNum == null && finishNum == null) {
      sum = 0;
    } else {
      sum = 1;
    }
    return sum;
  }
  
  console.log("==Soal No.3==")
  console.log(sum(1, 10));
  console.log(sum(5, 50, 2));
  console.log(sum(15, 10));
  console.log(sum(20, 10, 2));
  console.log(sum(1));
  console.log(sum());
  console.log();  


//==SOAL No.4==
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(input) {
    var data=0;
    while (data<input.length){
        console.log("Nomor ID: " + (input[data][0]));
        console.log("Nama Lengkap: " + (input[data][1]));
        console.log("TTL: " + (input[data][2]) + " " + (input[data][3]));
        console.log("Hobi: " + (input[data][4]) + "\n");
        data++
    }

    return ;
}

console.log("==Soal No.4==")
dataHandling(input);
console.log();


//==SOAL No.5==
function balikKata(str) {
    var sebelumbalik = str;
    var sesudah = "";
    for (var i = str.length - 1; i >= 0; i--) {
      sesudah = sesudah + sebelumbalik[i];
    }
  
    return sesudah;
  }
  console.log("==Soal No.5==")
  console.log(balikKata("Kasur Rusak"));
  console.log(balikKata("SanberCode"));
  console.log(balikKata("Haji Ijah"));
  console.log(balikKata("racecar"));
  console.log(balikKata("I am Sanbers"));
  console.log();


  //==SOAL No.6==
  function dataHandling2(text) {
    text.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    var texts = text;
    texts.splice(4, 1, "Pria", "SMA International Metro");
    console.log(texts);
    var date = texts[3];
    var getMonth = date.split("/");
    var month = getMonth[1];
  
    switch (month) {
      case "01": {month = "Januari";
      break;}
      case "02": {month = "Februari";
      break;}
      case "03": {month = "Maret ";
      break;}
      case "04": {month = "April ";
      break;}
      case "05": {month = "Mei ";
      break;}
      case "06": {month = "Juni ";
      break;}
      case "07": {month = "Juli ";
      break;}
      case "08": {month = "Agustus ";
      break;}
      case "09": {month = "September ";
      break;}
      case "10": {month = "Oktober ";
      break;}
      case "11": {month = "November ";
      break;}
      case "12": {month = "Desember ";
      break;}
      default: {
      }
    }
    console.log(month);
    var showMonth = getMonth.sort(function (value1, value2) {
      return value2 - value1;
    });
    console.log(showMonth);
    console.log(text[3].split("/").join("-"));
    console.log(texts[1].slice(0, 15));
  }
  
  var input = [
    "0001",
    "Roman Alamsyah ",
    "Bandar Lampung",
    "21/05/1989",
    "Membaca",
  ];
  console.log("==Soal No.6==")
  dataHandling2(input);
  console.log();
  
  