//==SOAL No.1==
function teriak() {
   var halo = "Halo Sanbers"
   return halo;
}
 
console.log(teriak()); // "Halo Sanbers!" 
console.log();


//==SOAL No.2==
function kalikan(num1,num2) {
    var hasil = num1*num2;
    hasil.toString();
    return hasil;
}
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48
console.log();


//==SOAL No.3==
function introduce(name, age, address, hobby) {
    var intro = ("Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!");
    return intro;
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 