//==SOAL No.1==
function arrayToObject(arr) {
    if (arr.length <= 0) {
        return console.log("")
      }
      for (var i = 0; i < arr.length; i++) {
        var newObject = {}

        var birthYear = arr[i][3];
        var now = new Date().getFullYear()
        var newAge;
    
        if (birthYear && now - birthYear > 0) {
          newAge = now - birthYear
        } else {
          newAge = 'Invalid Birth Year'
        }
    
        newObject.firstName = arr[i][0]
        newObject.lastName = arr[i][1]
        newObject.gender = arr[i][2]
        newObject.age = newAge
    
        var consoleText = (i + 1) + '. ' + newObject.firstName + ' ' + newObject.lastName + ': '
    
        console.log(consoleText)
        console.log(newObject)
      }
    
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
console.log("=#=#=Soal No.1=#=#=")
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
console.log();


//==SOAL No.2==
function shoppingTime(memberId, money) {
    // you can only write your code here!
    if (!memberId) {
      return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
      return "Mohon maaf, uang tidak cukup"
    } else {
      var newObject = {};
      var moneyChange = money;
      var purchasedList = [];
      var sepatuStacattu = 'Sepatu Stacattu';
      var bajuZoro = 'Baju Zoro';
      var bajuHn = 'Baju H&N';
      var sweaterUniklooh = 'Sweater Uniklooh';
      var casingHandphone = 'Casing Handphone';
      var check = 0;
      for (var i = 0; moneyChange >= 50000 && check == 0; i++) {
        if (moneyChange >= 1500000) {
          purchasedList.push(sepatuStacattu)
          moneyChange -= 1500000
        } else if (moneyChange >= 500000) {
          purchasedList.push(bajuZoro)
          moneyChange -= 500000
        } else if (moneyChange >= 250000) {
          purchasedList.push(bajuHn)
          moneyChange -= 250000
        } else if (moneyChange >= 175000) {
          purchasedList.push(sweaterUniklooh)
          moneyChange -= 175000
        } else if (moneyChange >= 50000) {
          if (purchasedList.length != 0) {
            for (var j = 0; j <= purchasedList.length - 1; j++) {
              if (purchasedList[j] == casingHandphone) {
                check += 1
              }
            }
            if (check == 0) {
              purchasedList.push(casingHandphone)
              moneyChange -= 50000
            }
          } else {
            purchasedList.push(casingHandphone)
            moneyChange -= 50000
          }
        }
      }
      newObject.memberId = memberId
      newObject.money = money
      newObject.listPurchased = purchasedList
      newObject.changeMoney = moneyChange
      return newObject
    }
  }
  
  // TEST CASES
  console.log("=#=#==Soal No.2==#=#=")
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log();
  

//==SOAL No.3==
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrOutput = []
    //your code here
    if (arrPenumpang.length <= 0) {
      return []
    }
    for (var i = 0; i < arrPenumpang.length; i++) {
      var objOutput = {}
      var asal = arrPenumpang[i][1]
      var tujuan = arrPenumpang[i][2]
      var indexAsal, indexTujuan;
      for (var j = 0; j < rute.length; j++) {
        if (rute[j] == asal) {
          indexAsal = j
        } else if (rute[j] == tujuan) {
          indexTujuan = j
        }
      }
      var bayar = (indexTujuan - indexAsal) * 2000
      objOutput.penumpang = arrPenumpang[i][0]
      objOutput.naikDari = asal
      objOutput.tujuan = tujuan
      objOutput.bayar = bayar
      arrOutput.push(objOutput)
    }
    return arrOutput
  }
  
  //TEST CASE
  console.log("=#=#==Soal No.3==#=#=")
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
  console.log(naikAngkot([])); //[]
  console.log();