
//==SOAL No.1==
var nomor = 2;
var loop = 1;
console.log("LOOPING PERTAMA")
while(nomor < 21) { 
    console.log(nomor+" - I love coding");
    nomor+=2; 
}

console.log("LOOPING KEDUA")
while(nomor>1){ 
    if(nomor>20){
        nomor=20;
    }
    console.log(nomor+" - I will become a mobile developer");
    nomor-=2; 
}
console.log();


//==SOAL No.2==
var i;
var nomor=1;
for (i=1;i<=20;i++){
    if(i%3==0 && i%2==1){
        console.log(i+" - I Love Coding")
    }else if(i%2==0){ 
        console.log(i+" - Berkualitas")
    }else if(i%2==1){   
        console.log(i+" - Santai")
    }
}
console.log();


//==SOAL No.3==
var baris=1;
while(baris<5){
  for(var persegipanjang="#";persegipanjang.length<8;persegipanjang+="#"){
  }
  console.log(persegipanjang);
  baris++;
}
console.log();


//==SOAL No.4==
for (var tangga = "#"; tangga.length <= 7; tangga += "#") {
    console.log(tangga);
}
console.log();


//==SOAL No.5==
var hitam='#';
var putih=' ';
var papancatur='';
for(var x=0;x<8;x++){
    for(var y=0;y<8;y++){
        if(x%2==1){
            if(y%2==0){
                papancatur+=hitam;
            }
            else{
                papancatur+=putih;
            }
        }
        else{
            if(y%2==0){
                papancatur+=putih;
            }
            else{
                papancatur+=hitam;
            }
        }
    }
    console.log(papancatur);
    papancatur='';
}

